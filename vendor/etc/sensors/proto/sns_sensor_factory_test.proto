// @file sns_sensor_factory_test.proto
//
// Defines test API message types for factory test.
//
// All physical Sensor drivers are required to use this API to support
// factory test. 
//
// Copyright (c) 2021-2021Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Technologies, Inc.

syntax = "proto2";
import "nanopb.proto";

enum sns_sensor_factory_test_msgid
{
  option (nanopb_enumopt).long_names = false;

  // diag request
  SNS_SENSOR_FACTORY_TEST_MSGID_DIAG_REQ = 516;

  // rgb cali diag request
  SNS_SENSOR_FACTORY_TEST_MSGID_DIAG_RGB_CALI_REQ  = 519;

  // diag response
  SNS_SENSOR_FACTORY_TEST_MSGID_DIAG_RSP  = 1027;
}

// Supported diag types for physical sensors
enum sns_sensor_factory_test_type
{
  option (nanopb_enumopt).long_names = false;

  // get als raw channel data.
  SNS_SENSOR_FACTORY_TEST_GET_ALS_RAW_DATA = 0;

  // get sensor info.
  SNS_PHYSICAL_SENSOR_TEST_GET_ALS_INFO = 1;

  // get sensor info.
  SNS_PHYSICAL_SENSOR_TEST_GET_ALS_COEF = 2;

  // get rgb raw channel data.
  SNS_SENSOR_FACTORY_TEST_GET_RGB_RAW_DATA = 3;

  // set rgb cali
  SNS_SENSOR_FACTORY_TEST_SET_RGB_CALI = 4;

  // get rgbc cali ratio
  SNS_SENSOR_FACTORY_TEST_GET_RGB_CALI_RATIO = 5;

  SNS_SENSOR_FACTORY_TEST_SET_ALS_HIGH_SAMPLE = 6;

  SNS_SENSOR_FACTORY_TEST_CLEAR_ALS_HIGH_SAMPLE = 7;
  // close acc&gyro
  SNS_SENSOR_FACTORY_TEST_CLOSE_ACC_GYRO = 8;
}

// Physical Sensor test request
message sns_sensor_factory_test_req
{
  // Requested test type.
  required sns_sensor_factory_test_type test_type = 1;
  // request data to process
  optional string req_data = 2;
}

// Sensor test rsp
message sns_sensor_factory_test_rsp
{
  // Result if the test execution was successful:
  // true for success
  // false for failure
  required bool test_passed = 1 [default = true];

  // test_type from sns_sensor_factory_test_type that
  // this event corresponds to
  required sns_sensor_factory_test_type test_type = 2 [default = SNS_SENSOR_FACTORY_TEST_GET_ALS_RAW_DATA];

  // Driver specific test data
  optional string data = 3;

  // Driver specific test data
  repeated float als_coef = 4;
}

// Sensor test rsp
message sns_sensor_factory_test_rgb_cali_target
{
  // rgb cali target
  repeated float target = 1;
}
